//
//  SphereMesh.h
//  VE
//
//  Created by Adrian Krupa on 19.10.2014.
//  Copyright (c) 2014 Adrian Krupa. All rights reserved.
//

#ifndef __VE__SphereMesh__
#define __VE__SphereMesh__

#include "Mesh.h"

class SphereMesh : public Mesh {
    
public:
    SphereMesh();
};
#endif /* defined(__VE__SphereMesh__) */
