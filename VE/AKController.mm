//
//  AKController.m
//  VE
//
//  Created by Adrian Krupa on 20.10.2014.
//  Copyright (c) 2014 Adrian Krupa. All rights reserved.
//

#import "AKController.h"
#import "SpringSimulator.h"
#import "ForceGraph.h"
#import "VelocityGraph.h"
#import "TrajectoryGraph.h"
#import "FunctionType.h"

@interface AKController () {
    SpringSimulator *_springSimulator;
}

@end


@implementation AKController

-(NSString*)stringFromFunctionType:(int)f
{
    static NSString *strings[] = { @"empty", @"Const", @"Sgn", @"Sin"};
    
    return strings[f];
}

- (id)init
{
    self = [super init];
    if (self) {
        delta = 60;
        _springSimulator = new SpringSimulator();
        _springSimulator -> SetSimulationConstans(0.5, 0, 3, 1.0/delta);
        _springSimulator -> SetSpringPosition(-1);
        NSTimer *dataTimer = [NSTimer timerWithTimeInterval:1.0 / delta
                                            target:self
                                          selector:@selector(updateData:)
                                          userInfo:nil
                                           repeats:YES];
        [[NSRunLoop mainRunLoop] addTimer:dataTimer forMode:NSRunLoopCommonModes];
    }
    

    
    return self;
}

-(void)awakeFromNib {
    [super awakeFromNib];
    _forceGraph->frameRate = delta;
    _forceGraph->step = 1.0/delta;
    _velocityGraph->frameRate = delta;
    _velocityGraph->step = 1.0/delta;
    
    [_popupH removeAllItems];
    [_popupW removeAllItems];
    for(int i=1;i<=3;i++) {
        [_popupH addItemWithTitle:[self stringFromFunctionType:i]];
        [_popupW addItemWithTitle:[self stringFromFunctionType:i]];
    }
    [self updateParam];
}

-(void)updateData:(NSTimer *)theTimer {
    float position = _springSimulator -> SimulationStep();

    [_openGLView SetSpringPosition:position];
    [_openGLView SetPlanePosition:_springSimulator->getWValue()];
    
    
    [self.forceGraph addValue:_springSimulator->getFValue() ToPlot:F];
    [self.forceGraph addValue:_springSimulator->getGValue() ToPlot:G];
    [self.forceGraph addValue:_springSimulator->getHValue() ToPlot:H];
    [self.forceGraph addValue:_springSimulator->getWValue() ToPlot:W];
    
    [self.forceGraph updateGraph];
    
    [self.velocityGraph addValue:_springSimulator->getXValue() ToPlot:X];
    [self.velocityGraph addValue:_springSimulator->getXtValue() ToPlot:Xt];
    [self.velocityGraph addValue:_springSimulator->getXttValue() ToPlot:Xtt];
    
    [self.velocityGraph updateGraph];
    
    [self.trajectoryGraph addValue:_springSimulator->getXValue() andValue:_springSimulator->getXtValue()];
    [self.trajectoryGraph updateGraph];

}

- (IBAction)popupHChanged:(id)sender {
    if ([self.popupH indexOfSelectedItem] == Sin -1|| [self.popupH indexOfSelectedItem] == Sgn-1) {
        [self.hOmegaTextField setEnabled:TRUE];
        [self.hPhiTextField setEnabled:TRUE];
    } else {
        [self.hOmegaTextField setEnabled:FALSE];
        [self.hPhiTextField setEnabled:FALSE];
    }
}

- (IBAction)popupWChanged:(id)sender {
    if ([self.popupW indexOfSelectedItem] == Sin -1|| [self.popupW indexOfSelectedItem] == Sgn-1) {
        [self.wOmegaTextField setEnabled:TRUE];
        [self.wPhiTextField setEnabled:TRUE];
    } else {
        [self.wOmegaTextField setEnabled:FALSE];
        [self.wPhiTextField setEnabled:FALSE];
    }
}

- (IBAction)resetSimulationClicked:(id)sender {
    _springSimulator -> ResetSimulation();
    [self.forceGraph resetGraph];
    [self.velocityGraph resetGraph];
    [self.trajectoryGraph resetGraph];
    [self updateParam];
}

- (IBAction)updateParameters:(id)sender {
    [self updateParam];
}

-(void)updateParam {
    _springSimulator->SetSimulationConstans([_mTextField floatValue], [_kTextField floatValue], [_cTextField floatValue], 1/[_deltaTextField floatValue]);
    
    _springSimulator->setHFunctionType((FunctionType)([self.popupH indexOfSelectedItem]+1), [self.hATextField floatValue], [self.hOmegaTextField floatValue], [self.hPhiTextField floatValue]);
    
    _springSimulator->setWFunctionType((FunctionType)([self.popupW indexOfSelectedItem]+1), [self.wATextField floatValue], [self.wOmegaTextField floatValue], [self.wPhiTextField floatValue]);

    
}

@end
