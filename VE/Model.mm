//
//  Model.cpp
//  Robot
//
//  Created by Bartosz Ciechanowski on 01/05/14.
//  Copyright (c) 2014 AKBC. All rights reserved.
//

#include "Model.h"
#include "WallShader.h"
#include "glm/gtc/type_ptr.hpp"

Model::Model::Model(Mesh *mesh, Shader::Shader *shader) : m_mesh(mesh), m_transformMatrix(glm::mat4()),shader(shader)
{
        
        meshBuffer = new Buffer::MeshBuffer();
        meshBuffer->setup();
        
        meshBuffer->fillVertexData(mesh->vertexes.data(),
                               mesh->vertexes.size() * sizeof(Vertex),
                               0);
        
        meshBuffer->fillIndexData((unsigned int *)mesh->triangles.data(),
                              mesh->triangles.size() * sizeof(Triangle),
                              0);
        
 }



glm::mat4 Model::Model::getTransformMatrix()
{
    return m_transformMatrix;
}

void Model::Model::setTransformMatrix(glm::mat4 matrix)
{
    m_transformMatrix = matrix;
}

void Model::Model::draw(glm::mat4 viewMatrix, glm::mat4 projectionMatrix)
{
    Shader::WallShader *m_shader = (Shader::WallShader*)shader;
    glUseProgram(shader->getProgram());
    
    glUniformMatrix4fv(m_shader->getViewMatrixUniform(), 1, 0, glm::value_ptr(viewMatrix));
    glUniformMatrix4fv(m_shader->getViewProjectionMatrixUniform(), 1, 0, glm::value_ptr(projectionMatrix*viewMatrix));
    
    //glUniform4fv(m_shader->getLightPositionUniform(), 1, glm::value_ptr(m_lightPosition));
    
    
    glBindVertexArray(meshBuffer->getVAO());
    
    GLuint indexCount = (GLuint)(m_mesh->triangles.size() * 3);
    glUniform4fv(m_shader->getDrawColorUniform(), 1, glm::value_ptr(color));
    
    glUniformMatrix4fv(m_shader->getModelMatrixUniform(), 1, 0, glm::value_ptr(m_transformMatrix));
        
    glDrawElements(GL_TRIANGLES, indexCount, GL_UNSIGNED_INT, 0);
    
    glUseProgram(0);
    glBindVertexArray(0);
}