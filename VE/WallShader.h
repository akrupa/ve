//
//  WallShader.h
//  VE
//
//  Created by Adrian Krupa on 17.10.2014.
//  Copyright (c) 2014 Adrian Krupa. All rights reserved.
//

#ifndef __VE__WallShader__
#define __VE__WallShader__

#include <stdio.h>
#include "Shader.h"

namespace Shader {
    class WallShader : public Shader {
        
    private:
        GLuint m_viewProjectionMatrixUniform;
        GLuint m_viewMatrixUniform;
        GLuint m_modelMatrixUniform;
        GLuint m_lightPositionUniform;
        
        GLuint m_drawColorUniform;
        
        virtual std::string getShaderName();
        
        virtual void bindAttributeLocations();
        virtual void fetchUniformLocations();
    public:
        GLuint getViewProjectionMatrixUniform();
        GLuint getViewMatrixUniform();
        GLuint getModelMatrixUniform();
        GLuint getLightPositionUniform();
        GLuint getDrawColorUniform();
    };
    
}

#endif /* defined(__VE__WallShader__) */
