//
//  CubeMesh.m
//  VE
//
//  Created by Adrian Krupa on 20.10.2014.
//  Copyright (c) 2014 Adrian Krupa. All rights reserved.
//

#import "CubeMesh.h"


CubeMesh::CubeMesh() : Mesh()
{
    Vertex v;
    v.normal = glm::vec3(0.0f, 0.0f, 1.0f);
    
    v.position = glm::vec3(-1.0f, 1.0f, 1.0f);
    vertexes.push_back(v);
    
    v.position = glm::vec3( 1.0f, 1.0f, 1.0f);
    vertexes.push_back(v);
    
    v.position = glm::vec3( 1.0f,-1.0f, 1.0f);
    vertexes.push_back(v);
    
    v.position = glm::vec3(-1.0f,-1.0f, 1.0f);
    vertexes.push_back(v);
    
    v.position = glm::vec3(-1.0f, 1.0f, -1.0f);
    vertexes.push_back(v);
    
    v.position = glm::vec3( 1.0f, 1.0f, -1.0f);
    vertexes.push_back(v);
    
    v.position = glm::vec3( 1.0f,-1.0f, -1.0f);
    vertexes.push_back(v);
    
    v.position = glm::vec3(-1.0f,-1.0f, -1.0f);
    vertexes.push_back(v);
    
    
    Triangle t;
    
    t.indices.a = 0;
    t.indices.b = 1;
    t.indices.c = 2;
    triangles.push_back(t);
    
    t.indices.a = 0;
    t.indices.b = 2;
    t.indices.c = 3;
    triangles.push_back(t);
    
    t.indices.a = 0;
    t.indices.b = 1;
    t.indices.c = 4;
    triangles.push_back(t);
    
    t.indices.a = 1;
    t.indices.b = 4;
    t.indices.c = 5;
    triangles.push_back(t);
    
    t.indices.a = 5;
    t.indices.b = 1;
    t.indices.c = 6;
    triangles.push_back(t);
    
    t.indices.a = 6;
    t.indices.b = 2;
    t.indices.c = 1;
    triangles.push_back(t);
    
    t.indices.a = 6;
    t.indices.b = 2;
    t.indices.c = 3;
    triangles.push_back(t);
    
    t.indices.a = 3;
    t.indices.b = 6;
    t.indices.c = 7;
    triangles.push_back(t);
    
    t.indices.a = 4;
    t.indices.b = 6;
    t.indices.c = 7;
    triangles.push_back(t);
    
    t.indices.a = 4;
    t.indices.b = 6;
    t.indices.c = 5;
    triangles.push_back(t);
    
    t.indices.a = 3;
    t.indices.b = 6;
    t.indices.c = 4;
    triangles.push_back(t);
    
    t.indices.a = 1;
    t.indices.b = 4;
    t.indices.c = 6;
    triangles.push_back(t);
    
}
