//
//  Mesh.h
//  Robot
//
//  Created by Bartosz Ciechanowski on 01/05/14.
//  Copyright (c) 2014 AKBC. All rights reserved.
//

#ifndef __Robot__Mesh__
#define __Robot__Mesh__

#include <iostream>
#include <vector>

#include "Vertex.h"


struct Triangle {
    union indices {
        struct
        {
            unsigned int a,b,c;
        };
        unsigned int v[3];
    } indices;
};



struct Adj {
    unsigned int adj[6];
};

class Mesh {
    
public:
    std::vector<Vertex> vertexes;
    std::vector<Triangle> triangles;

    std::vector<Vertex> positions;
    std::vector<Adj> adjs;
    
    Mesh();
    Mesh(std::string fileName);
};

#endif /* defined(__Robot__Mesh__) */
