//
//  RealTimeGraph.m
//  VE
//
//  Created by Adrian Krupa on 04.10.2014.
//  Copyright (c) 2014 Adrian Krupa. All rights reserved.
//

#import "RealTimeGraph.h"

@implementation RealTimeGraph

-(void)awakeFromNib {
    
    //step = 0.5;
    graphSize = 10;
    
    [super awakeFromNib];
    
    dataSources = [[NSMutableArray alloc]init];
    dataPlots = [[NSMutableArray alloc]init];
    modifiedPlots = [[NSMutableArray alloc]init];
    
    // Create graph from theme
    CGRect frame = [hostView bounds];
    graph = [[CPTXYGraph alloc] initWithFrame:frame];
    
    CPTTheme *theme = [CPTTheme themeNamed:kCPTDarkGradientTheme];
    [graph applyTheme:theme];
    
    graph.paddingLeft = 3.0;
    graph.paddingTop = 3.0;
    graph.paddingRight = 3.0;
    graph.paddingBottom = 3.0;
    
    graph.plotAreaFrame.paddingTop = 10.0f;
    graph.plotAreaFrame.paddingRight = 10.0f;
    graph.plotAreaFrame.paddingBottom = 30.0f;
    graph.plotAreaFrame.paddingLeft = 40.0f;
    
    graph.plotAreaFrame.borderLineStyle = nil;
    
    
    // Create grid line styles
    CPTMutableLineStyle *majorGridLineStyle = [CPTMutableLineStyle lineStyle];
    majorGridLineStyle.lineWidth = 1.0;
    majorGridLineStyle.lineColor = [[CPTColor whiteColor] colorWithAlphaComponent:0.75];
    
    CPTMutableLineStyle *minorGridLineStyle = [CPTMutableLineStyle lineStyle];
    minorGridLineStyle.lineWidth = 1.0;
    minorGridLineStyle.lineColor = [[CPTColor whiteColor] colorWithAlphaComponent:0.25];
    
    CPTXYPlotSpace *plotSpace = (CPTXYPlotSpace *)graph.defaultPlotSpace;
    
    plotSpace.xRange = [CPTPlotRange plotRangeWithLocation:CPTDecimalFromUnsignedInteger(0)
                                                    length:CPTDecimalFromUnsignedInteger(graphSize)];
    
    plotSpace.yRange = [CPTPlotRange plotRangeWithLocation:CPTDecimalFromInteger(-6)
                                                    length:CPTDecimalFromUnsignedInteger(12)];
    
    NSNumberFormatter *labelFormatter = [[NSNumberFormatter alloc] init];
    labelFormatter.numberStyle = NSNumberFormatterNoStyle;
    
    // Set axes
    CPTXYAxisSet *axisSet = (CPTXYAxisSet *)graph.axisSet;
    CPTXYAxis *x = axisSet.xAxis;
    x.majorIntervalLength = CPTDecimalFromInteger(5);
    x.labelingPolicy = CPTAxisLabelingPolicyAutomatic;
    x.minorTicksPerInterval = 10;
    x.orthogonalCoordinateDecimal = CPTDecimalFromUnsignedInteger(0);
    x.majorGridLineStyle = majorGridLineStyle;
    x.minorGridLineStyle = minorGridLineStyle;
    x.minorTicksPerInterval = 10;
    //x.title = @"X Axis";
    //x.titleOffset = 35.0;
    x.axisLineStyle = nil;
    x.labelOffset = 60.0f;
    x.labelFormatter = labelFormatter;
    
    CPTXYAxis *y = axisSet.yAxis;
    y.labelingPolicy = CPTAxisLabelingPolicyAutomatic;
    y.orthogonalCoordinateDecimal = CPTDecimalFromUnsignedInteger(0);
    y.majorGridLineStyle = majorGridLineStyle;
    y.minorGridLineStyle = minorGridLineStyle;
    y.minorTicksPerInterval = 3;
    y.labelOffset = 5.0;
    //y.title = @"Y Axis";
    //y.titleOffset = 30.0;
    y.axisConstraints = [CPTConstraints constraintWithLowerOffset:0.0];
    y.labelFormatter = labelFormatter;
    
    
    hostView.hostedGraph = graph;
    
    currentIndex = 0;
}

-(void)updateGraph {
    NSInteger max = 0;
    for (int i=0; i < [modifiedPlots count]; i++) {
        NSInteger value = [modifiedPlots[i] integerValue];
        max=max>=value?max:value;
    }
    currentIndex+=step*max;
    [self updateLocation];

    for (int i=0; i < [modifiedPlots count]; i++) {
        if ((NSNumber*)modifiedPlots[i] > 0) {
            NSInteger size = [(NSNumber*)modifiedPlots[i] integerValue];
            [[dataPlots objectAtIndex:i] insertDataAtIndex:[[dataSources objectAtIndex:i]count] - size numberOfRecords:size];
            modifiedPlots[i]=[NSNumber numberWithInt:0];
        }
    }
}

-(void)updateLocation {
    float kMaxDataPoints = graphSize/step+1;
    float location = 0;
    float maximumValue = 3;
    CPTXYPlotSpace *plotSpace = (CPTXYPlotSpace *)graph.defaultPlotSpace;
    for (int i=0; i < [dataPlots count]; i++) {
        if ([graph plotWithIdentifier:[[dataPlots objectAtIndex:i]identifier]] != nil) {
            CPTPlot *thePlot = [dataPlots objectAtIndex:i];
            NSMutableArray *plotData = [dataSources objectAtIndex:i];

            for (NSNumber *data in plotData) {
                maximumValue = ABS([data floatValue]) > maximumValue ? ABS([data floatValue]) : maximumValue;
            }
            
            if ( plotData.count >= kMaxDataPoints) {
                [plotData removeObjectAtIndex:0];
                [thePlot deleteDataInIndexRange:NSMakeRange(0, 1)];
            }
            
            location = MAX(location, (currentIndex >= graphSize ? currentIndex - graphSize + step : 0));
        }
    }
    
    CPTPlotRange *oldRange = [CPTPlotRange plotRangeWithLocation:CPTDecimalFromFloat( (location > 0) ? (location - step) : 0 )
                                                          length:CPTDecimalFromUnsignedInteger(graphSize)];
    CPTPlotRange *newRange = [CPTPlotRange plotRangeWithLocation:CPTDecimalFromFloat(location)
                                                          length:CPTDecimalFromUnsignedInteger(graphSize)];
    
    plotSpace.yRange = [CPTPlotRange plotRangeWithLocation:CPTDecimalFromInteger(-maximumValue)
                                                    length:CPTDecimalFromUnsignedInteger(maximumValue*2)];
    
    [CPTAnimation animate:plotSpace
                 property:@"xRange"
            fromPlotRange:oldRange
              toPlotRange:newRange
                 duration:CPTFloat(1.0 / frameRate)];
}

-(void)addPlotWithIdentifier:(NSString*)identifier andColor:(CPTColor*)color {
    CPTScatterPlot *dataSourceLinePlot = [[CPTScatterPlot alloc] init];
    dataSourceLinePlot.identifier     = identifier;
    dataSourceLinePlot.cachePrecision = CPTPlotCachePrecisionDouble;
    
    CPTMutableLineStyle *lineStyle = [dataSourceLinePlot.dataLineStyle mutableCopy];
    lineStyle.lineWidth              = 1.0;
    lineStyle.lineColor              = color;
    dataSourceLinePlot.dataLineStyle = lineStyle;
    
    dataSourceLinePlot.dataSource = self;
    [graph addPlot:dataSourceLinePlot];
    [dataSources addObject:[[NSMutableArray alloc]initWithCapacity:graphSize/step]];
    [dataPlots addObject:dataSourceLinePlot];
    [modifiedPlots addObject:[NSNumber numberWithInt:0]];
    
    // Add legend
    graph.legend                 = [CPTLegend legendWithGraph:graph];
    graph.legend.swatchSize      = CGSizeMake(25.0, 25.0);
    graph.legend.numberOfRows    = 1;
    graph.legend.delegate        = self;
    graph.legendAnchor           = CPTRectAnchorBottom;
    
    CPTMutableTextStyle *textStyle =[[CPTMutableTextStyle alloc] init];
    [textStyle setColor:[CPTColor whiteColor]];
    [[graph legend] setTextStyle:(CPTTextStyle *)textStyle];

}

-(void)resetGraph {
    currentIndex = 0;
    
    CPTXYPlotSpace *plotSpace = (CPTXYPlotSpace *)graph.defaultPlotSpace;
    plotSpace.xRange = [CPTPlotRange plotRangeWithLocation:CPTDecimalFromUnsignedInteger(0)
                                                    length:CPTDecimalFromUnsignedInteger(graphSize)];
    
    for (int i=0; i < [dataPlots count]; i++) {
        NSMutableArray *plotData = [dataSources objectAtIndex:i];
        CPTPlot *thePlot = [dataPlots objectAtIndex:i];
        [thePlot deleteDataInIndexRange:NSMakeRange(0, [plotData count])];
        [plotData removeAllObjects];
        [thePlot reloadData];
    }
    
}

-(NSUInteger)numberOfRecordsForPlot:(CPTPlot *)plot
{
    for (int i=0; i<[dataSources count]; i++) {
        if ([[graph plotAtIndex:i]identifier] == [plot identifier]) {
            NSMutableArray *plotData = [dataSources objectAtIndex:i];
            return [plotData count];
        }
    }
    return 0;
}

-(int)getIndexForPlot:(CPTPlot *)plot {
    for (int i=0; i<[dataSources count]; i++) {
        if ([[graph plotAtIndex:i]identifier] == [plot identifier]) {
            return i;
        }
    }
    return 0;
}

-(id)numberForPlot:(CPTPlot *)plot field:(NSUInteger)fieldEnum recordIndex:(NSUInteger)index
{
    NSNumber *num = nil;
    NSMutableArray *plotData = [dataSources objectAtIndex:[self getIndexForPlot:plot]];
    
    switch ( fieldEnum ) {
        case CPTScatterPlotFieldX:
            num = [NSNumber numberWithDouble:(index*step + currentIndex - plotData.count*step)];
            break;
            
        case CPTScatterPlotFieldY:
            num = plotData[index];
            break;
            
        default:
            break;
    }
    
    return num;
}

#pragma mark - Legend delegate

-(void)legend:(CPTLegend *)legend legendEntryForPlot:(CPTPlot *)plot wasSelectedAtIndex:(NSUInteger)idx
{
    plot.hidden = !plot.hidden;
}

-(NSString *)legendTitleForBarPlot:(CPTBarPlot *)barPlot recordIndex:(NSUInteger)index
{
    return [NSString stringWithFormat:@"Bar %lu", (unsigned long)(index + 1)];
}

@end
