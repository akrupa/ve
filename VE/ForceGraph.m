//
//  ForceGraph.m
//  VE
//
//  Created by Adrian Krupa on 04.10.2014.
//  Copyright (c) 2014 Adrian Krupa. All rights reserved.
//

#import "ForceGraph.h"

@implementation ForceGraph

static NSString *const kPlotFIdentifier = @"f(t)";
static NSString *const kPlotGIdentifier = @"g(t)";
static NSString *const kPlotHIdentifier = @"h(t)";
static NSString *const kPlotWIdentifier = @"w(t)";

-(void)awakeFromNib
{
    [super awakeFromNib];
    
    [self addPlotWithIdentifier:kPlotFIdentifier andColor:[CPTColor greenColor]];
    [self addPlotWithIdentifier:kPlotGIdentifier andColor:[CPTColor redColor]];
    [self addPlotWithIdentifier:kPlotHIdentifier andColor:[CPTColor blueColor]];
    [self addPlotWithIdentifier:kPlotWIdentifier andColor:[CPTColor grayColor]];
}

-(void)addValue:(double)value ToPlot:(ForcePlotType)plot {
    NSMutableArray *plotData = [dataSources objectAtIndex:plot];
    [plotData addObject:@(value)];
    modifiedPlots[plot]=[NSNumber numberWithLong:[modifiedPlots[plot] integerValue]+(long)1];
}

@end
