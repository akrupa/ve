//
//  SphereMesh.cpp
//  VE
//
//  Created by Adrian Krupa on 19.10.2014.
//  Copyright (c) 2014 Adrian Krupa. All rights reserved.
//

#include "SphereMesh.h"

SphereMesh::SphereMesh() : Mesh()
{
    /*
    Vertex v;
    v.normal = glm::vec3(0.0f, 0.0f, 1.0f);
    
    v.position = glm::vec3(-1.0f, 1.0f, 0.0f);
    vertexes.push_back(v);
    
    v.position = glm::vec3( 1.0f, 1.0f, 0.0f);
    vertexes.push_back(v);
    
    v.position = glm::vec3( 1.0f,-1.0f, 0.0f);
    vertexes.push_back(v);
    
    v.position = glm::vec3(-1.0f,-1.0f, 0.0f);
    vertexes.push_back(v);
    
    v.position = glm::vec3( 1.0f, 1.0f, -1.0f);
    vertexes.push_back(v);
    
    
    Triangle t;
    t.indices.a = 0;
    t.indices.b = 1;
    t.indices.c = 2;
    
    triangles.push_back(t);
    
    t.indices.a = 0;
    t.indices.b = 2;
    t.indices.c = 3;
    
    triangles.push_back(t);
    
    t.indices.a = 0;
    t.indices.b = 1;
    t.indices.c = 4;
    triangles.push_back(t);
     */
    
    Vertex vert;
    Triangle t;
    
    int uCount = 20;
    int vCount = 20;
    float r = 1;
    
    for (int u=0; u<uCount; u++) {
        for (int v=0; v<vCount; v++) {
            float uPar = u*M_2_PI/uCount;
            float vPar = v*M_PI/vCount;
            vert.position = glm::vec3(r*cos(uPar)*sin(vPar), r*sin(uPar)*sin(vPar), r*cos(vPar));
            vertexes.push_back(vert);
        }
    }
    
}