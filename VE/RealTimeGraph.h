//
//  RealTimeGraph.h
//  VE
//
//  Created by Adrian Krupa on 04.10.2014.
//  Copyright (c) 2014 Adrian Krupa. All rights reserved.
//

#import <CorePlot/CorePlot.h>

@interface RealTimeGraph : NSObject<CPTPlotDataSource,CPTLegendDelegate> {
    @public
    IBOutlet CPTGraphHostingView *hostView;
    CPTXYGraph *graph;
    NSMutableArray *dataSources;
    NSMutableArray *dataPlots;
    NSMutableArray *modifiedPlots;
    float currentIndex;
    float step;
    int frameRate;
    int graphSize;
}

-(void)updateGraph;
-(void)addPlotWithIdentifier:(NSString*)identifier andColor:(CPTColor*)color;
-(void)resetGraph;
//-(void)newData:(NSTimer *)theTimer;

@end
