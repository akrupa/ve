//
//  AKOpenGLView.m
//  VE
//
//  Created by Adrian Krupa on 15.10.2014.
//  Copyright (c) 2014 Adrian Krupa. All rights reserved.
//

#import "AKOpenGLView.h"
#import <math.h>
#import "OpenGL/gl3.h"
#import <vector>
#import "PlotType.h"

using namespace glm;

@implementation AKOpenGLView

- (CVReturn) getFrameForTime:(const CVTimeStamp*)outputTime
{
    [self drawView];
    return kCVReturnSuccess;
}

- (void) awakeFromNib {
    
    NSOpenGLPixelFormatAttribute attrs[] =
    {
        NSOpenGLPFADoubleBuffer,
        NSOpenGLPFADepthSize, 24,
        // Must specify the 3.2 Core Profile to use OpenGL 3.2
        NSOpenGLPFAOpenGLProfile,
        NSOpenGLProfileVersion3_2Core,
        0
    };
    
    NSOpenGLPixelFormat *pf = [[NSOpenGLPixelFormat alloc] initWithAttributes:attrs];
    
    if (!pf)
    {
        NSLog(@"No OpenGL pixel format");
    }
    
    NSOpenGLContext* context = [[NSOpenGLContext alloc] initWithFormat:pf shareContext:nil];
    
    CGLEnable(static_cast<CGLContextObj>([context CGLContextObj]), kCGLCECrashOnRemovedFunctions);
    
    [self setPixelFormat:pf];
    [self setOpenGLContext:context];
    [self setWantsBestResolutionOpenGLSurface:YES];
}

-(void)SetSpringPosition:(float)position {
    renderer.setSpringPosition(position);
    [self setNeedsDisplay:TRUE];
}

-(void)SetPlanePosition:(float)position {
    renderer.setPlanePosition(position);
    [self setNeedsDisplay:TRUE];
}



- (void) prepareOpenGL {
    [super prepareOpenGL];
    [self setAcceptsTouchEvents:YES];
    
    // Make all the OpenGL calls to setup rendering
    //  and build the necessary rendering objects
    [self initGL];
    /*
     // Create a display link capable of being used with all active displays
     CVDisplayLinkCreateWithActiveCGDisplays(&displayLink);
     
     // Set the renderer output callback function
     CVDisplayLinkSetOutputCallback(displayLink, &MyDisplayLinkCallback, (__bridge void *)(self));
     
     // Set the display link for the current renderer
     CGLContextObj cglContext = [[self openGLContext] CGLContextObj];
     CGLPixelFormatObj cglPixelFormat = [[self pixelFormat] CGLPixelFormatObj];
     CVDisplayLinkSetCurrentCGDisplayFromOpenGLContext(displayLink, cglContext, cglPixelFormat);
     
     // Activate the display link
     CVDisplayLinkStart(displayLink);
     
     // Register to be notified when the window closes so we can stop the displaylink
     [[NSNotificationCenter defaultCenter] addObserver:self
     selector:@selector(windowWillClose:)
     name:NSWindowWillCloseNotification
     object:[self window]];
     */
}

- (BOOL) acceptsFirstResponder {
    return YES;
}

- (void) windowWillClose:(NSNotification*)notification {
    
    CVDisplayLinkStop(displayLink);
}

- (void) initGL {
    [[self openGLContext] makeCurrentContext];
    
    GLint swapInt = 1;
    [[self openGLContext] setValues:&swapInt forParameter:NSOpenGLCPSwapInterval];
    
    renderer.setup();
    renderer.initScene();
}

- (void) drawRect: (NSRect) bounds {

    [self drawView];
}

- (void) drawView {
    [[self openGLContext] makeCurrentContext];
    CGLLockContext(static_cast<CGLContextObj>([[self openGLContext] CGLContextObj]));

    renderer.renderWithCameraController(&cameraController);
    
    CGLFlushDrawable(static_cast<CGLContextObj>([[self openGLContext] CGLContextObj]));
    CGLUnlockContext(static_cast<CGLContextObj>([[self openGLContext] CGLContextObj]));
}

- (void) reshape {
    [super reshape];
    CGLLockContext(static_cast<CGLContextObj>([[self openGLContext] CGLContextObj]));

    
    //[m_renderer resizeWithWidth:viewRectPixels.size.width
     //                 AndHeight:viewRectPixels.size.height];
    
    CGLUnlockContext(static_cast<CGLContextObj>([[self openGLContext] CGLContextObj]));
}

- (void) renewGState {
    [[self window] disableScreenUpdatesUntilFlush];
    //[super renewGStat];
}

- (void) mouseDragged:(NSEvent *)theEvent {
    cameraController.mouseDrag([theEvent deltaX], [theEvent deltaY]);
    
    
    [self setNeedsDisplay:TRUE];
}


- (void)keyDown:(NSEvent *)theEvent {
    [self interpretKeyEvents:[NSArray arrayWithObject:theEvent]];
}

- (void)insertText:(id)string {
    float speed = 1;
    float x=0,y=0,z=0;
    if([string rangeOfString:@"w"].location != NSNotFound) {
        z += speed/10;
    }
    if([string rangeOfString:@"W"].location != NSNotFound) {
        z += speed*4;
    }
    
    if([string rangeOfString:@"s"].location != NSNotFound) {
        z -= speed/10;
    }
    if([string rangeOfString:@"S"].location != NSNotFound) {
        z -= speed*4;
    }
    
    if([string rangeOfString:@"a"].location != NSNotFound) {
        x += speed;
    }
    if([string rangeOfString:@"A"].location != NSNotFound) {
        x += speed*20;
    }
    
    if([string rangeOfString:@"d"].location != NSNotFound) {
        x -= speed;
    }
    if([string rangeOfString:@"D"].location != NSNotFound) {
        x -= speed;
    }
    
    if([string rangeOfString:@"q"].location != NSNotFound) {
        y += speed;
    }
    if([string rangeOfString:@"Q"].location != NSNotFound) {
        y += speed;
    }
    
    if([string rangeOfString:@"e"].location != NSNotFound) {
        y -= speed;
    }
    if([string rangeOfString:@"E"].location != NSNotFound) {
        y -= speed;
    }
    
    cameraController.keyboardMove(x, y, z);
    [self setNeedsDisplay:TRUE];
}

@end
