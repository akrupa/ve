//
//  CubeMesh.h
//  VE
//
//  Created by Adrian Krupa on 20.10.2014.
//  Copyright (c) 2014 Adrian Krupa. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "Mesh.h"

#ifndef __Robot__CubeMesh__
#define __Robot__CubeMesh__

#include "Mesh.h"

class CubeMesh : public Mesh {
    
public:
    CubeMesh();
};

#endif /* defined(__Robot__PlaneMesh__) */
