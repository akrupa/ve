//
//  TrajectoryGraph.m
//  VE
//
//  Created by Adrian Krupa on 20.10.2014.
//  Copyright (c) 2014 Adrian Krupa. All rights reserved.
//

#import "TrajectoryGraph.h"

@implementation TrajectoryGraph

-(void)awakeFromNib {
    
    [super awakeFromNib];
    
    dataSources = [[NSMutableArray alloc]init];
    dataPlots = [[NSMutableArray alloc]init];
    modifiedPlots = [[NSMutableArray alloc]init];
    
    // Create graph from theme
    CGRect frame = [hostView bounds];
    graph = [[CPTXYGraph alloc] initWithFrame:frame];
    
    CPTTheme *theme = [CPTTheme themeNamed:kCPTDarkGradientTheme];
    [graph applyTheme:theme];
    
    graph.paddingLeft = 3.0;
    graph.paddingTop = 3.0;
    graph.paddingRight = 3.0;
    graph.paddingBottom = 3.0;
    
    graph.plotAreaFrame.paddingTop = 10.0f;
    graph.plotAreaFrame.paddingRight = 10.0f;
    graph.plotAreaFrame.paddingBottom = 30.0f;
    graph.plotAreaFrame.paddingLeft = 40.0f;
    
    graph.plotAreaFrame.borderLineStyle = nil;
    
    [self addPlotWithIdentifier:@"Trajectory" andColor:[CPTColor redColor]];
    
    
    // Create grid line styles
    CPTMutableLineStyle *majorGridLineStyle = [CPTMutableLineStyle lineStyle];
    majorGridLineStyle.lineWidth = 1.0;
    majorGridLineStyle.lineColor = [[CPTColor whiteColor] colorWithAlphaComponent:0.75];
    
    CPTMutableLineStyle *minorGridLineStyle = [CPTMutableLineStyle lineStyle];
    minorGridLineStyle.lineWidth = 1.0;
    minorGridLineStyle.lineColor = [[CPTColor whiteColor] colorWithAlphaComponent:0.25];
    
    CPTXYPlotSpace *plotSpace = (CPTXYPlotSpace *)graph.defaultPlotSpace;
    
    plotSpace.xRange = [CPTPlotRange plotRangeWithLocation:CPTDecimalFromInteger(-6)
                                                    length:CPTDecimalFromUnsignedInteger(12)];
    
    plotSpace.yRange = [CPTPlotRange plotRangeWithLocation:CPTDecimalFromInteger(-6)
                                                    length:CPTDecimalFromUnsignedInteger(12)];
    
    NSNumberFormatter *labelFormatter = [[NSNumberFormatter alloc] init];
    labelFormatter.numberStyle = NSNumberFormatterNoStyle;
    
    // Set axes
    CPTXYAxisSet *axisSet = (CPTXYAxisSet *)graph.axisSet;
    CPTXYAxis *x = axisSet.xAxis;
    x.majorIntervalLength = CPTDecimalFromInteger(5);
    x.labelingPolicy = CPTAxisLabelingPolicyAutomatic;
    x.minorTicksPerInterval = 10;
    x.orthogonalCoordinateDecimal = CPTDecimalFromUnsignedInteger(0);
    x.majorGridLineStyle = majorGridLineStyle;
    x.minorGridLineStyle = minorGridLineStyle;
    x.minorTicksPerInterval = 10;
    //x.title = @"X Axis";
    //x.titleOffset = 35.0;
    x.axisLineStyle = nil;
    x.labelOffset = 60.0f;
    x.labelFormatter = labelFormatter;
    
    CPTXYAxis *y = axisSet.yAxis;
    y.labelingPolicy = CPTAxisLabelingPolicyAutomatic;
    y.orthogonalCoordinateDecimal = CPTDecimalFromUnsignedInteger(0);
    y.majorGridLineStyle = majorGridLineStyle;
    y.minorGridLineStyle = minorGridLineStyle;
    y.minorTicksPerInterval = 3;
    y.labelOffset = 5.0;
    //y.title = @"Y Axis";
    //y.titleOffset = 30.0;
    y.axisConstraints = [CPTConstraints constraintWithLowerOffset:0.0];
    y.labelFormatter = labelFormatter;
    
    
    hostView.hostedGraph = graph;
    
}

-(NSUInteger)numberOfRecordsForPlot:(CPTPlot *)plot
{
    for (int i=0; i<[dataSources count]; i++) {
        if ([[graph plotAtIndex:i]identifier] == [plot identifier]) {
            NSMutableArray *plotData = [dataSources objectAtIndex:i];
            return [plotData count];
        }
    }
    return 0;
}

-(void)updateGraph {
    /*
    NSInteger max = 2;
    for (int i=0; i < 2; i++) {
        for (int j=0; j<[[dataSources objectAtIndex:i]count]; j++) {
            double value = ceil(ABS([(NSNumber*)[[dataSources objectAtIndex:i]objectAtIndex:j]doubleValue]));
            max=max>=value?max:value;
        }
    }
    
    CPTXYPlotSpace *plotSpace = (CPTXYPlotSpace *)graph.defaultPlotSpace;
    
    plotSpace.xRange = [CPTPlotRange plotRangeWithLocation:CPTDecimalFromInteger(-max)
                                                    length:CPTDecimalFromUnsignedInteger(2*max)];
    
    plotSpace.yRange = [CPTPlotRange plotRangeWithLocation:CPTDecimalFromInteger(-max)
                                                    length:CPTDecimalFromUnsignedInteger(2*max)];
    
    if ((NSNumber*)modifiedPlots[0] > 0) {
        NSInteger size = [(NSNumber*)modifiedPlots[0] integerValue];
        NSLog(@"Insert data at index: %lu - number if records: %ld",[[dataSources objectAtIndex:0]count] - size ,(long)size);
        [[dataPlots objectAtIndex:0] insertDataAtIndex:[[dataSources objectAtIndex:0]count] - size numberOfRecords:size];
        modifiedPlots[0]=[NSNumber numberWithInt:0];
    }
     */
}

-(id)numberForPlot:(CPTPlot *)plot field:(NSUInteger)fieldEnum recordIndex:(NSUInteger)index
{
    NSNumber *num = nil;
    
    switch ( fieldEnum ) {
        case CPTScatterPlotFieldX:
            num = [dataSources objectAtIndex:0][index];
            break;
            
        case CPTScatterPlotFieldY:
            num = [dataSources objectAtIndex:1][index];
            break;
            
        default:
            break;
    }
    
    return num;
}

-(void)addValue:(double)xValue andValue:(double)xtValue {
    [[dataSources objectAtIndex:0]addObject:@(xValue)];
    [[dataSources objectAtIndex:1]addObject:@(xtValue)];
    modifiedPlots[0]=[NSNumber numberWithLong:(long)xValue+(long)1];
    modifiedPlots[1]=[NSNumber numberWithLong:(long)xtValue+(long)1];

    NSInteger max = 2;
    for (int i=0; i < 2; i++) {
        for (int j=0; j<[[dataSources objectAtIndex:i]count]; j++) {
            double value = ceil(ABS([(NSNumber*)[[dataSources objectAtIndex:i]objectAtIndex:j]doubleValue]));
            max=max>=value?max:value;
        }
    }
    
    CPTXYPlotSpace *plotSpace = (CPTXYPlotSpace *)graph.defaultPlotSpace;
    
    plotSpace.xRange = [CPTPlotRange plotRangeWithLocation:CPTDecimalFromInteger(-max)
                                                    length:CPTDecimalFromUnsignedInteger(2*max)];
    
    plotSpace.yRange = [CPTPlotRange plotRangeWithLocation:CPTDecimalFromInteger(-max)
                                                    length:CPTDecimalFromUnsignedInteger(2*max)];
    
    if ((NSNumber*)modifiedPlots[0] > 0) {
        [[dataPlots objectAtIndex:0] insertDataAtIndex:[[dataSources objectAtIndex:0]count] - 1 numberOfRecords:1];
        modifiedPlots[0]=[NSNumber numberWithInt:0];
    }
}

-(void)addPlotWithIdentifier:(NSString*)identifier andColor:(CPTColor*)color {
    CPTScatterPlot *dataSourceLinePlot = [[CPTScatterPlot alloc] init];
    dataSourceLinePlot.identifier     = identifier;
    dataSourceLinePlot.cachePrecision = CPTPlotCachePrecisionDouble;
    
    CPTMutableLineStyle *lineStyle = [dataSourceLinePlot.dataLineStyle mutableCopy];
    lineStyle.lineWidth              = 1.0;
    lineStyle.lineColor              = color;
    dataSourceLinePlot.dataLineStyle = lineStyle;
    
    dataSourceLinePlot.dataSource = self;
    [graph addPlot:dataSourceLinePlot];
    [dataSources addObject:[[NSMutableArray alloc]init]];
    [dataSources addObject:[[NSMutableArray alloc]init]];
    [dataPlots addObject:dataSourceLinePlot];
    [modifiedPlots addObject:[NSNumber numberWithInt:0]];
    [modifiedPlots addObject:[NSNumber numberWithInt:0]];
    
    // Add legend
    graph.legend                 = [CPTLegend legendWithGraph:graph];
    graph.legend.swatchSize      = CGSizeMake(25.0, 25.0);
    graph.legend.numberOfRows    = 1;
    graph.legend.delegate        = self;
    graph.legendAnchor           = CPTRectAnchorBottom;
    
    CPTMutableTextStyle *textStyle =[[CPTMutableTextStyle alloc] init];
    [textStyle setColor:[CPTColor whiteColor]];
    [[graph legend] setTextStyle:(CPTTextStyle *)textStyle];
    
}

-(void)resetGraph {
    
    CPTXYPlotSpace *plotSpace = (CPTXYPlotSpace *)graph.defaultPlotSpace;
    plotSpace.xRange = [CPTPlotRange plotRangeWithLocation:CPTDecimalFromInteger(-6)
                                                    length:CPTDecimalFromUnsignedInteger(2*6)];
    plotSpace.yRange = [CPTPlotRange plotRangeWithLocation:CPTDecimalFromInteger(-6)
                                                    length:CPTDecimalFromUnsignedInteger(2*6)];
    CPTPlot *thePlot = [dataPlots objectAtIndex:0];
    NSMutableArray *plotData = [dataSources objectAtIndex:0];
    [thePlot deleteDataInIndexRange:NSMakeRange(0, [plotData count])];

    for (int i=0; i < 2; i++) {
        NSMutableArray *plotData = [dataSources objectAtIndex:i];
        [plotData removeAllObjects];
        [thePlot reloadData];
    }
    
}

@end
