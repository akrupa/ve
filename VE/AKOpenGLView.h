//
//  AKOpenGLView.h
//  VE
//
//  Created by Adrian Krupa on 15.10.2014.
//  Copyright (c) 2014 Adrian Krupa. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <QuartzCore/CVDisplayLink.h>
#import "Renderer.h"
#import "CameraController.h"

@interface AKOpenGLView : NSOpenGLView {
    CVDisplayLinkRef displayLink;
    Renderer::Renderer renderer;
    Controller::CameraController cameraController;
    NSTimer *dataTimer;
    float delta;
}

-(void)SetSpringPosition:(float)position;
-(void)SetPlanePosition:(float)position;

@end
