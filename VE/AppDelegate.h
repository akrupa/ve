//
//  AppDelegate.h
//  VE
//
//  Created by Adrian Krupa on 03.10.2014.
//  Copyright (c) 2014 Adrian Krupa. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@class AKController;


@interface AppDelegate : NSObject <NSApplicationDelegate>

@property (nonatomic, strong) IBOutlet AKController *controller;


@end

