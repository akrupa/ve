//
//  VelocityGraph.m
//  VE
//
//  Created by Adrian Krupa on 20.10.2014.
//  Copyright (c) 2014 Adrian Krupa. All rights reserved.
//

#import "VelocityGraph.h"

@implementation VelocityGraph

static NSString *const kPlotXIdentifier = @"x(t)";
static NSString *const kPlotXtIdentifier = @"xt(t)";
static NSString *const kPlotXttIdentifier = @"xtt(t)";

-(void)awakeFromNib
{
    [super awakeFromNib];
    
    [self addPlotWithIdentifier:kPlotXIdentifier andColor:[CPTColor greenColor]];
    [self addPlotWithIdentifier:kPlotXtIdentifier andColor:[CPTColor redColor]];
    [self addPlotWithIdentifier:kPlotXttIdentifier andColor:[CPTColor blueColor]];
}

-(void)addValue:(double)value ToPlot:(SpeedPlotType)plot {
    NSMutableArray *plotData = [dataSources objectAtIndex:plot];
    [plotData addObject:@(value)];
    modifiedPlots[plot]=[NSNumber numberWithLong:[modifiedPlots[plot] integerValue]+(long)1];
}

@end
