//
//  Renderer.h
//  Robot
//
//  Created by Bartosz Ciechanowski on 23/04/14.
//  Copyright (c) 2014 AKBC. All rights reserved.
//

#ifndef __Robot__Renderer__
#define __Robot__Renderer__

#import <OpenGL/gl3.h>
#import "glm.hpp"
#import <vector>
#import "Model.h"
#import "CameraController.h"

namespace Renderer {
    class Renderer {
        std::vector<Model::Model *> models;
        Model::Model *spring;
        Model::Model *plane;
    public:
        void setup();
        void initScene();
        void renderScene();
        void renderWithCameraController(Controller::CameraController *camera);
        void setSpringPosition(float position);
        void setPlanePosition(float position);
    };
}

#endif /* defined(__Robot__Renderer__) */
