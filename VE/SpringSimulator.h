//
//  springSimulator.h
//  VE
//
//  Created by Adrian Krupa on 19.10.2014.
//  Copyright (c) 2014 Adrian Krupa. All rights reserved.
//

#ifndef __VE__springSimulator__
#define __VE__springSimulator__

#include <stdio.h>
#import "FunctionType.h"

class SpringSimulator {
    
    float m;
    float k;
    float c;
    float delta;
    
    float f=0;
    float g=0;
    float h=0;
    float w=0;
    
    float x;
    float xDelta;
    float xMinusDelta;
    
    float time = 0;
    
    FunctionType Htype;
    FunctionType Wtype;
    
    float haValue;
    float hphiValue;
    float homegaValue;
    
    float waValue;
    float wphiValue;
    float womegaValue;
    
public:
    SpringSimulator();
    void SetSimulationConstans(float m, float k, float c, float delta);
    void SetSpringPosition(float position);
    void ResetSimulation();
    float SimulationStep();
    
    float getFValue();
    float getGValue();
    float getHValue();
    float getWValue();
    
    float getXValue();
    float getXtValue();
    float getXttValue();
    
    void setHFunctionType(FunctionType type, float constAValue, float omegaValue, float phiValue);
    void setWFunctionType(FunctionType type, float constAValue, float omegaValue, float phiValue);
};

#endif /* defined(__VE__springSimulator__) */
