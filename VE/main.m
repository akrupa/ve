//
//  main.m
//  VE
//
//  Created by Adrian Krupa on 03.10.2014.
//  Copyright (c) 2014 Adrian Krupa. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[]) {
    return NSApplicationMain(argc, argv);
}
