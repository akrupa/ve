//
//  ForceGraph.h
//  VE
//
//  Created by Adrian Krupa on 04.10.2014.
//  Copyright (c) 2014 Adrian Krupa. All rights reserved.
//

#import "RealTimeGraph.h"
#import "PlotType.h"

@interface ForceGraph : RealTimeGraph

-(void)addValue:(double)value ToPlot:(ForcePlotType)plot;

@end
