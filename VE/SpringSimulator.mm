//
//  springSimulator.cpp
//  VE
//
//  Created by Adrian Krupa on 19.10.2014.
//  Copyright (c) 2014 Adrian Krupa. All rights reserved.
//

#include "SpringSimulator.h"
#include <math.h>

void SpringSimulator::SetSimulationConstans(float _m, float _k, float _c, float _delta) {
    m = _m;
    k = _k;
    c = _c;
    delta = _delta;
}

void SpringSimulator::ResetSimulation() {
    x = 0;
    xDelta = 0;
    xMinusDelta = 0;
    time=0;
}

SpringSimulator::SpringSimulator() {
    ResetSimulation();
}

float SpringSimulator::SimulationStep() {
    switch (Htype) {
        case Const:
        {
            h=haValue;
            break;
        }
        case Sgn:
        {
            float val = sin(homegaValue*time+hphiValue);
            h = haValue * (val>0?1:-1);
            break;
        }
        case Sin:
        {
            h=haValue * sin(homegaValue*time+hphiValue);
            break;
        }
    }
    
    switch (Wtype) {
        case Const:
        {
            w=waValue;
            break;
        }
        case Sgn:
        {
            float val = sin(womegaValue*time+wphiValue);
            w = waValue * (val>0?1:-1);
            break;
        }
        case Sin:
        {
            w=waValue * sin(womegaValue*time+wphiValue);
            break;
        }
    }
    
    xDelta = (2*delta*delta*h + 2*c*delta*delta*w + delta*k*xMinusDelta - 2*m*xMinusDelta - 2*c*delta*delta*x + 4*m*x)/
    (delta*k + 2*m);
    
    f = c*(w - x);
    g= -k*(xDelta-xMinusDelta)/(2*delta);

    xMinusDelta = x;
    x = xDelta;
    
    time += delta;
    
    return xDelta;
}

void SpringSimulator::SetSpringPosition(float position) {
    x = position;
    xMinusDelta = position;
}

float SpringSimulator::getFValue() {
    return f;
}

float SpringSimulator::getGValue() {
    return g;
}

float SpringSimulator::getHValue() {
    return h;
}

float SpringSimulator::getWValue() {
    return w;
}

float SpringSimulator::getXValue() {
    return x;
}

float SpringSimulator::getXtValue() {
    return (xDelta-xMinusDelta)/(2*delta);
}

float SpringSimulator::getXttValue() {
    return (xDelta - 2*x + xMinusDelta)/(delta*delta);
}

void SpringSimulator::setHFunctionType(FunctionType type, float constAValue, float omegaValue, float phiValue) {
    this -> Htype = type;
    this -> haValue = constAValue;
    this -> hphiValue = phiValue;
    this -> homegaValue = omegaValue;
}

void SpringSimulator::setWFunctionType(FunctionType type, float constAValue, float omegaValue, float phiValue) {
    this -> Wtype = type;
    this -> waValue = constAValue;
    this -> wphiValue = phiValue;
    this -> womegaValue = omegaValue;
}

