//
//  PlotType.h
//  VE
//
//  Created by Adrian Krupa on 06.10.2014.
//  Copyright (c) 2014 Adrian Krupa. All rights reserved.
//

#ifndef VE_PlotType_h
#define VE_PlotType_h

typedef enum {
    F = 0,
    G,
    H,
    W
} ForcePlotType;

typedef enum {
    X = 0,
    Xt,
    Xtt
} SpeedPlotType;

#endif
