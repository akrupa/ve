//
//  WallShader.cpp
//  VE
//
//  Created by Adrian Krupa on 17.10.2014.
//  Copyright (c) 2014 Adrian Krupa. All rights reserved.
//

#include "WallShader.h"


std::string Shader::WallShader::getShaderName()
{
    return "WallShader";
}

void Shader::WallShader::bindAttributeLocations()
{
    glBindAttribLocation(m_program, VertexAttribPosition, "in_Position");
    glBindAttribLocation(m_program, VertexAttribNormal, "in_Normal");
    
}

void Shader::WallShader::fetchUniformLocations()
{
    m_viewProjectionMatrixUniform = glGetUniformLocation(m_program, "viewProjectionMatrix");
    m_viewMatrixUniform = glGetUniformLocation(m_program, "viewMatrix");
    m_modelMatrixUniform = glGetUniformLocation(m_program, "modelMatrix");
    
    m_lightPositionUniform = glGetUniformLocation(m_program, "lightPosition");
    m_drawColorUniform = glGetUniformLocation(m_program, "drawColor");
}



GLuint Shader::WallShader::getViewProjectionMatrixUniform()
{
    return m_viewProjectionMatrixUniform;
}

GLuint Shader::WallShader::getDrawColorUniform()
{
    return m_drawColorUniform;
}

GLuint Shader::WallShader::getViewMatrixUniform()
{
    return m_viewMatrixUniform;
}

GLuint Shader::WallShader::getLightPositionUniform()
{
    return m_lightPositionUniform;
}


GLuint Shader::WallShader::getModelMatrixUniform()
{
    return m_modelMatrixUniform;
}