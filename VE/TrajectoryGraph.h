//
//  TrajectoryGraph.h
//  VE
//
//  Created by Adrian Krupa on 20.10.2014.
//  Copyright (c) 2014 Adrian Krupa. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CorePlot/CorePlot.h>

@interface TrajectoryGraph : NSObject<CPTPlotDataSource,CPTLegendDelegate> {
    @public
    IBOutlet CPTGraphHostingView *hostView;
    CPTXYGraph *graph;
    NSMutableArray *dataSources;
    NSMutableArray *dataPlots;
    NSMutableArray *modifiedPlots;
    int frameRate;
}

-(void)updateGraph;
-(void)addPlotWithIdentifier:(NSString*)identifier andColor:(CPTColor*)color;
-(void)addValue:(double)xValue andValue:(double)xtValue;
-(void)resetGraph;

@end
