//
//  Renderer.cpp
//  Robot
//
//  Created by Bartosz Ciechanowski on 23/04/14.
//  Copyright (c) 2014 AKBC. All rights reserved.
//

#import "Renderer.h"
#import "Mesh.h"
#import "type_ptr.hpp"
#import "matrix_transform.hpp"
#import "Model.h"
#import "CameraController.h"
#import "PlaneMesh.h"
#import "CubeMesh.h"
#import "SphereMesh.h"
#import "WallShader.h"

using namespace glm;

void Renderer::Renderer::setup()
{
    
    glClearColor(0.2, 0.2, 0.2, 1.0);
    glClearStencil(0);
}

void Renderer::Renderer::initScene() {
    CubeMesh *wallMesh = new CubeMesh();
    
    mat4 translation = translate(mat4(), vec3(0.0f, 0.0f, 0.0f));
    
    Shader::WallShader *shader = new Shader::WallShader();
    shader->setup();
    Model::Model * wall = new Model::Model(wallMesh, shader);
    
    float scale = 0.1f;

    wall->setTransformMatrix(translation* glm::scale(mat4(), vec3(scale, scale, scale)));
    wall->color = vec4(0.0f, 1.0f, 0.0f, 1.0f);
    models.push_back(wall);
    spring = wall;
    
    PlaneMesh *planeMesh = new PlaneMesh();
    Model::Model *planeModel = new Model::Model(planeMesh, shader);
    
    planeModel->setTransformMatrix(translation* glm::scale(mat4(), vec3(scale, scale, scale)));
    planeModel->color = vec4(1.0f, 0.0f, 0.0f, 1.0f);
    models.push_back(planeModel);
    plane = planeModel;
    /*
    SphereMesh *sphereMesh = new SphereMesh();
    Model::Model * sphere = new Model::Model(sphereMesh, shader);
    sphere->setTransformMatrix(translation* glm::scale(mat4(), vec3(scale, scale, scale)));
    sphere->color = vec4(0.0f, 0.3f, 0.0f, 1.0f);
    models.push_back(sphere);
     */
}

void Renderer::Renderer::setSpringPosition(float position) {
    float scale = 0.1f;
    spring->setTransformMatrix(translate(mat4(), vec3(0.0f, position, 0.0f)) * glm::scale(mat4(), vec3(scale, scale, scale)));
}

void Renderer::Renderer::setPlanePosition(float position) {
    float scale = 0.1f;
    plane->setTransformMatrix(translate(mat4(), vec3(0.0f, position, 0.0f)) * glm::scale(mat4(), vec3(scale, scale, scale)));
}

void Renderer::Renderer::renderWithCameraController(Controller::CameraController *camera){
    
    glPolygonMode( GL_FRONT_AND_BACK, GL_LINE );

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
    
    glEnable(GL_DEPTH_TEST);
    glDepthMask(GL_TRUE);
    
    glDisable(GL_CULL_FACE);
    
    //glEnable(GL_CULL_FACE);
    
    for (auto model : models) {
        model->draw(camera->getViewMatrix(), camera->getProjectionMatrix());
    }
    
    glPolygonMode( GL_FRONT_AND_BACK, GL_FILL );

    
    //glCullFace(GL_BACK);
}
