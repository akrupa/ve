//
//  AKController.h
//  VE
//
//  Created by Adrian Krupa on 20.10.2014.
//  Copyright (c) 2014 Adrian Krupa. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AKOpenGLView.h"

@class ForceGraph, AKOpenGLView, VelocityGraph, TrajectoryGraph;

@interface AKController : NSObject {
    int delta;
}

@property (nonatomic, strong) IBOutlet AKOpenGLView *openGLView;
@property (nonatomic, strong) IBOutlet ForceGraph *forceGraph;
@property (nonatomic, strong) IBOutlet VelocityGraph *velocityGraph;
@property (nonatomic, strong) IBOutlet TrajectoryGraph *trajectoryGraph;
@property (nonatomic, strong) IBOutlet NSPopUpButton *popupW;
@property (nonatomic, strong) IBOutlet NSPopUpButton *popupH;

@property (nonatomic, strong) IBOutlet NSTextField *mTextField;
@property (nonatomic, strong) IBOutlet NSTextField *cTextField;
@property (nonatomic, strong) IBOutlet NSTextField *kTextField;
@property (nonatomic, strong) IBOutlet NSTextField *deltaTextField;

@property (nonatomic, strong) IBOutlet NSTextField *hATextField;
@property (nonatomic, strong) IBOutlet NSTextField *hOmegaTextField;
@property (nonatomic, strong) IBOutlet NSTextField *hPhiTextField;

@property (nonatomic, strong) IBOutlet NSTextField *wATextField;
@property (nonatomic, strong) IBOutlet NSTextField *wOmegaTextField;
@property (nonatomic, strong) IBOutlet NSTextField *wPhiTextField;
@end
