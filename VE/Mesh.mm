//
//  Mesh.cpp
//  Robot
//
//  Created by Bartosz Ciechanowski on 01/05/14.
//  Copyright (c) 2014 AKBC. All rights reserved.
//

#include "Mesh.h"

#include <fstream>

using namespace std;
using namespace glm;

Mesh::Mesh()
{
    vertexes = vector<Vertex>();
    triangles = vector<Triangle>();
    
    adjs = vector<Adj>();
    positions = vector<Vertex>();

    
}

Mesh::Mesh(std::string fileName) : Mesh()
{
    vector<int> vertexesToPositions = vector<int>();

    
    std::ifstream file(fileName);
    
    
    int positionsCount;
    file >> positionsCount;
    
    for (int i = 0; i < positionsCount; i++) {
        vec3 position;

        file >> position.x >> position.y >> position.z;
        
        Vertex posV;
        posV.position = position;
        positions.push_back(posV);
    }
    
    
    int verticiesCount;
    file >> verticiesCount;

    for (int i = 0; i < verticiesCount; i++) {
        unsigned int index;
        Vertex v;
        file >> index >> v.normal.x >> v.normal.y >> v.normal.z;
        v.position = positions[index].position;
        
        vertexes.push_back(v);
        vertexesToPositions.push_back(index);
    }
    
    
    
    int trianglesCount;
    file >> trianglesCount;
    
    for (int i = 0; i < trianglesCount; i++) {
        Triangle t;
        file  >> t.indices.v[0] >> t.indices.v[1] >> t.indices.v[2];
        triangles.push_back(t);
        
        Adj a;
        a.adj[0] = vertexesToPositions[t.indices.v[0]];
        a.adj[2] = vertexesToPositions[t.indices.v[1]];
        a.adj[4] = vertexesToPositions[t.indices.v[2]];
        adjs.push_back(a);
    }
    
    int edgesCount;
    file >> edgesCount;
    
    for (int i = 0; i < edgesCount; i++) {
        unsigned int vertexPosIndex[2];
        unsigned int triangleIndex[2];
        
        file >> vertexPosIndex[0] >> vertexPosIndex[1] >> triangleIndex[0] >> triangleIndex[1];
        
        Triangle t1 = triangles[triangleIndex[0]];
        Triangle t2 = triangles[triangleIndex[1]];
        
        int firstTriangleCommonVertIndex[2] = {-1, -1};
        int secondTriangleCommonVertIndex[2] = {-1, -1};
        
        int * c1 = firstTriangleCommonVertIndex;
        int * c2 = secondTriangleCommonVertIndex;
        
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (vertexesToPositions[t1.indices.v[i]] == vertexesToPositions[t2.indices.v[j]]) {
                    *c1 = i;
                    *c2 = j;
                    c1++;
                    c2++;
                }
            }
        }
        
        assert(c1 == &firstTriangleCommonVertIndex[2]);
        assert(c2 == &secondTriangleCommonVertIndex[2]);
        
        Adj a1 = adjs[triangleIndex[0]];
        Adj a2 = adjs[triangleIndex[1]];
        
        //a1.adj[2*firstTriangleCommonVertIndex[0]] = vertexPosIndex[0];
        //a1.adj[2*firstTriangleCommonVertIndex[1]] = vertexPosIndex[1];

        //a2.adj[2*secondTriangleCommonVertIndex[0]] = vertexPosIndex[0];
        //a2.adj[2*secondTriangleCommonVertIndex[1]] = vertexPosIndex[1];
        
        int firstTriangleOtherVert = 3 - firstTriangleCommonVertIndex[0] - firstTriangleCommonVertIndex[1];
        int secondTriangleOtherVert = 3 - secondTriangleCommonVertIndex[0] - secondTriangleCommonVertIndex[1];
        
        int map[3] = {3, 5, 1};
        
        a1.adj[map[firstTriangleOtherVert]] = vertexesToPositions[t2.indices.v[secondTriangleOtherVert]];
        a2.adj[map[secondTriangleOtherVert]] = vertexesToPositions[t1.indices.v[firstTriangleOtherVert]];
        
        adjs[triangleIndex[0]] = a1;
        adjs[triangleIndex[1]] = a2;
    }
    
    for (auto a : adjs) {
        for (int i = 0; i < 6; i++) {
            assert(a.adj[i] !=-1);
        }
    }
}