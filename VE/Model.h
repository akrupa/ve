//
//  Model.h
//  Robot
//
//  Created by Bartosz Ciechanowski on 01/05/14.
//  Copyright (c) 2014 AKBC. All rights reserved.
//

#ifndef __Robot__Model__
#define __Robot__Model__

#include "glm/glm.hpp"

#include "Mesh.h"
#include "Shader.h"
#include "MeshBuffer.h"

namespace Model {
    class Model {
        glm::mat4 m_transformMatrix;
        Mesh *m_mesh;
        Shader::Shader *shader;
        Buffer::MeshBuffer *meshBuffer;
        
        virtual void eat() {}
    public:
        glm::vec4 color;

        Model(Mesh *mesh, Shader::Shader *shader);
        
        glm::mat4 getTransformMatrix();
        void setTransformMatrix(glm::mat4 matrix);
        void draw(glm::mat4 viewMatrix, glm::mat4 projectionMatrix);
    };
}

#endif /* defined(__Robot__Model__) */
