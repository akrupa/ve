//
//  FunctionType.h
//  VE
//
//  Created by Adrian Krupa on 22.10.2014.
//  Copyright (c) 2014 Adrian Krupa. All rights reserved.
//

#ifndef VE_FunctionType_h
#define VE_FunctionType_h

typedef enum functionType {
    Const = 1,
    Sgn = 2,
    Sin = 3
} FunctionType;


#endif
