//
//  VelocityGraph.h
//  VE
//
//  Created by Adrian Krupa on 20.10.2014.
//  Copyright (c) 2014 Adrian Krupa. All rights reserved.
//

#import "RealTimeGraph.h"
#import "PlotType.h"

@interface VelocityGraph : RealTimeGraph

-(void)addValue:(double)value ToPlot:(SpeedPlotType)plot;

@end
